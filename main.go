package main

import (
	"log"
	"os"
)

func main() {
	query := os.Args[1]
	path := os.Args[2]
	server := NewServer()
	defer server.Stop()
	server.StartWatching(Path(path))
	r, err := server.Search(query)
	if err != nil {
		log.Println(err)
		return
	}
	for path, lines := range r {
		log.Println(path, lines)
	}
	log.Println("reverse index size:", server.Size())
}
