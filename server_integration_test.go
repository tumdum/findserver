package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"
)

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}

func mustTempDir() Path {
	name, err := ioutil.TempDir(".", "")
	panicOnErr(err)
	return Path(name)
}

func mustCreateNewFile(root Path, name, content string) Path {
	panicOnErr(os.MkdirAll(string(root), os.ModePerm))
	path := string(root) + "/" + name
	panicOnErr(ioutil.WriteFile(path, []byte(content), os.ModePerm))
	path, err := filepath.Abs(path)
	panicOnErr(err)
	return Path(path)
}

func waitFor(t *testing.T, pred func() bool) {
	t.Helper()
	start := time.Now()
	for {
		diff := time.Now().Sub(start)
		if diff > time.Second {
			t.Fatalf("Time limit for waiting exceeded")
		}
		if pred() {
			return
		}
		time.Sleep(time.Millisecond)
	}
}

func waitForQueryMinSize(t *testing.T, server *Server, query string, size int) {
	t.Helper()
	waitFor(t, func() bool { r, _ := server.search(query); return len(r) > size })
}

func TestWatchRecursiveNewFiles(t *testing.T) {
	root := mustTempDir()
	defer os.RemoveAll(string(root))
	server := NewServer()
	defer server.Stop()
	panicOnErr(server.startWatching(root))
	path := mustCreateNewFile(root, "test.txt", "foo bar baz")

	waitForQueryMinSize(t, server, "bar", 0)

	result, err := server.search("bar")
	panicOnErr(err)
	expected := Locations{Line(0)}
	assertEq(t, expected, result[path])

	path2 := mustCreateNewFile(root, "test2.txt", "foo bar baz")

	waitForQueryMinSize(t, server, "bar", 1)

	result, err = server.search("bar")
	panicOnErr(err)
	assertEq(t, expected, result[path])
	assertEq(t, expected, result[path2])
}

func TestRemovingWatchedFileRemovesItFromResults(t *testing.T) {
	root := mustTempDir()
	defer os.RemoveAll(string(root))
	server := NewServer()
	defer server.Stop()
	panicOnErr(server.startWatching(root))

	path := mustCreateNewFile(root, "test.txt", "foo bar baz")

	waitForQueryMinSize(t, server, "bar", 0)

	os.Remove(string(path))

	waitFor(t, func() bool { r, _ := server.search("bar"); return len(r) == 0 })

	for _, query := range []string{"foo", "bar", "baz"} {
		result, err := server.search(query)
		panicOnErr(err)
		if len(result[path]) != 0 {
			t.Fatalf("Expected empty result for '%v', got '%v'", query, result)
		}
	}
}

func TestChangingFileContentShouldRemoveOldEntriesAndAddNewOnes(t *testing.T) {
	root := mustTempDir()
	defer os.RemoveAll(string(root))
	server := NewServer()
	defer server.Stop()
	panicOnErr(server.startWatching(root))

	mustCreateNewFile(root, "test.txt", "foo bar baz")

	waitForQueryMinSize(t, server, "bar", 0)

	path := mustCreateNewFile(root, "test.txt", "aa bb cc dd\nbar")

	waitForQueryMinSize(t, server, "aa", 0)

	for _, query := range []string{"aa", "bb", "cc", "dd"} {
		result, err := server.search(query)
		panicOnErr(err)
		expected := Locations{Line(0)}
		assertEq(t, expected, result[path])
	}
	result, err := server.search("bar")
	panicOnErr(err)
	assertEq(t, Locations{Line(1)}, result[path])

	for _, query := range []string{"foo", "baz"} {
		result, err := server.search(query)
		panicOnErr(err)
		if len(result[path]) != 0 {
			t.Fatalf("Expected empty result for '%v', got '%v'", query, result)
		}
	}
}

func TestExistingFilesShouldBeIndexedWhenWatchingIsStarted(t *testing.T) {
	root := mustTempDir()
	defer os.RemoveAll(string(root))
	server := NewServer()
	defer server.Stop()

	panicOnErr(server.startWatching(root))
	if server.startWatching(root) != ErrPathAlreadyWatched {
		t.Fatalf("duplicate path not detected")
	}

	path1 := mustCreateNewFile(root, "test.txt", "foo bar baz")
	path2 := mustCreateNewFile(root+"/a/b/c", "test2.txt", "aoeui idhtns fgcrl")
	waitForQueryMinSize(t, server, "bar", 0)
	waitForQueryMinSize(t, server, "aoeui", 0)

	for query, path := range map[string]Path{"foo": path1, "bar": path1, "baz": path1, "aoeui": path2, "idhtns": path2, "fgcrl": path2} {
		result, err := server.search(query)
		panicOnErr(err)
		expected := Locations{Line(0)}
		assertEq(t, expected, result[path])
	}
}

func TestAllSubdirsWalkedDuringStartWatchingShouldBeWatched(t *testing.T) {
	root := mustTempDir()
	defer os.RemoveAll(string(root))
	server := NewServer()
	defer server.Stop()

	path2 := mustCreateNewFile(root+"/a/b/c", "test2.txt", "aoeui idhtns fgcrl")

	panicOnErr(server.startWatching(root))

	path1 := mustCreateNewFile(root+"/a/b/c", "test.txt", "foo bar baz")
	waitForQueryMinSize(t, server, "bar", 0)
	waitForQueryMinSize(t, server, "aoeui", 0)

	for query, path := range map[string]Path{"foo": path1, "bar": path1, "baz": path1, "aoeui": path2, "idhtns": path2, "fgcrl": path2} {
		result, err := server.search(query)
		panicOnErr(err)
		expected := Locations{Line(0)}
		assertEq(t, expected, result[path])
	}
}
