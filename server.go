package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/rjeczalik/notify"
)

var (
	ErrPathAlreadyWatched = errors.New("path is already watched")
	ErrIsDir              = errors.New("is dir")
)

type Path string

type Source interface {
	Open() (io.ReadCloser, error)
	Path() Path
}

type SearchResult map[Path]Locations

type Server struct {
	// storage      Storage
	index        *Index
	changes      chan notify.EventInfo
	workerCancel context.CancelFunc
	watchedDirs  map[Path]struct{}
	knownFiles   map[Path]struct{}
	readTasks    chan func()
	mtx          sync.Mutex
}

func NewServer() *Server {
	ctx, cancel := context.WithCancel(context.Background())
	server := Server{
		// storage:      make(Storage),
		index:        NewIndex(),
		changes:      make(chan notify.EventInfo, 10),
		watchedDirs:  make(map[Path]struct{}),
		knownFiles:   make(map[Path]struct{}),
		readTasks:    make(chan func()),
		workerCancel: cancel}
	for i := 0; i < runtime.NumCPU()*2; i++ {
		go server.consumeEvents(ctx)
	}
	return &server
}

func (server *Server) Stop() {
	server.workerCancel()
	notify.Stop(server.changes)
}

func (server *Server) consumeEvents(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case f := <-server.readTasks:
			f()
		case e := <-server.changes:
			switch e.Event() {
			case notify.Create, notify.Write:
				server.AddSource(&pathSource{Path(e.Path())})
			case notify.Remove:
				server.RemoveSource(Path(e.Path()))
			}
		}
	}
}

type pathSource struct {
	path Path
}

func (s *pathSource) Open() (io.ReadCloser, error) {
	f, err := os.Open(string(s.path))
	if err != nil {
		return nil, err
	}
	stat, err := f.Stat()
	if err != nil {
		return nil, err
	}
	if stat.IsDir() {
		return nil, ErrIsDir
	}
	return f, nil
}

func (s *pathSource) Path() Path {
	return s.path
}

func (server *Server) AddSource(source Source) error {
	r, err := source.Open()
	if err != nil {
		if err == ErrIsDir {
			server.startWatching(source.Path())
			return nil
		}
		return fmt.Errorf("adding new source failed: %v", err)
	}
	defer r.Close()
	readerIndex, _ := server.index.indexFromReader(r)
	server.mtx.Lock()
	defer server.mtx.Unlock()
	server.removeSourceUnlocked(source.Path())
	server.addReaderIndex(readerIndex, source.Path())
	return nil
}

func (server *Server) addReaderIndex(index ReaderIndex, path Path) {
	server.knownFiles[path] = struct{}{}
	server.index.SetDocument(index, path)
}

func (server *Server) RemoveSource(path Path) {
	server.mtx.Lock()
	defer server.mtx.Unlock()
	server.removeSourceUnlocked(path)
}

func (server *Server) removeSourceUnlocked(path Path) {
	_, known := server.knownFiles[path]
	if !known {
		return
	}
	server.index.RemoveDocument(path)
	delete(server.knownFiles, path)
}

func (server *Server) StartWatching(root Path) error {
	defer timeTrack(time.Now(), "StartWatching")
	return server.startWatching(root)
}

func (server *Server) startWatching(root Path) error {
	if err := server.registerPath(root); err != nil {
		return err
	}

	var wg sync.WaitGroup
	visitor := func(path string, info os.FileInfo, err error) error {
		path, err = filepath.Abs(path)
		if err != nil {
			return nil
		}
		if !info.IsDir() {
			wg.Add(1)
			server.readTasks <- func() {
				server.AddSource(&pathSource{Path(path)})
				wg.Done()
			}
		} else {
			server.registerPath(Path(path))
		}
		return nil
	}
	err := filepath.Walk(string(root), visitor)
	wg.Wait()
	return err
}

func (server *Server) registerPath(path Path) error {
	server.mtx.Lock()
	defer server.mtx.Unlock()
	if _, known := server.watchedDirs[path]; known {
		return ErrPathAlreadyWatched
	}
	if err := notify.Watch(string(path), server.changes, notify.All); err != nil {
		return err
	}
	server.watchedDirs[path] = struct{}{}
	return nil
}

func (server *Server) Search(query string) (SearchResult, error) {
	defer timeTrack(time.Now(), "Search")
	return server.search(query)
}

// TODO: add path under which to search for
func (server *Server) search(query string) (SearchResult, error) {
	query = normalize(query)
	// NOTE for know assume literal search, in future regexp will be needed.
	// TODO query does not need to be single word
	if len(strings.Fields(query)) != 1 {
		return nil, errors.New("multi word queries are not (yet?) supported")
	}
	server.mtx.Lock()
	defer server.mtx.Unlock()
	wordMap, _ := server.index.Lookup(Word(query))
	clone := make(SearchResult)
	for path, locations := range wordMap {
		clone[path] = append(Locations{}, locations...)
	}
	return clone, nil
}

func (server *Server) Size() int {
	return server.index.Size()
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}
