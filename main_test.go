package main

import (
	"reflect"
	"strings"
	"testing"
)

func failTestOnError(t *testing.T, err error) {
	if err != nil {
		t.Errorf("unexpected error '%v'", err)
	}
}

func assertEq(t *testing.T, expected interface{}, val interface{}) {
	t.Helper()
	if !reflect.DeepEqual(expected, val) {
		t.Fatalf("Expected '%v', got '%v'", expected, val)
	}
}

func TestReaderIndexFromEmptyReaderIsEmpty(t *testing.T) {
	index, err := NewIndex().indexFromReader(strings.NewReader(""))
	failTestOnError(t, err)
	if len(index) != 0 {
		t.Fatalf("Expected empty index, get '%v'", index)
	}
}

func TestReaderIndexContainsAllWordsFromOneLineInput(t *testing.T) {
	index, err := NewIndex().indexFromReader(strings.NewReader("foo bar baz"))
	failTestOnError(t, err)
	assertEq(t, Locations{Line(0)}, index["foo"])
	assertEq(t, Locations{Line(0)}, index["bar"])
	assertEq(t, Locations{Line(0)}, index["baz"])
}

func TestReaderIndexMapsWordToLineAtMostOnce(t *testing.T) {
	index, err := NewIndex().indexFromReader(strings.NewReader("foo foo foo"))
	failTestOnError(t, err)
	assertEq(t, Locations{Line(0)}, index["foo"])
}

func TestReaderIndexContainsAllWordsFromMultipleLinesInput(t *testing.T) {
	index, err := NewIndex().indexFromReader(strings.NewReader("foo bar baz\nfoo\nbar baz\nbaz foo"))
	failTestOnError(t, err)
	assertEq(t, Locations{Line(0), Line(1), Line(3)}, index["foo"])
	assertEq(t, Locations{Line(0), Line(2)}, index["bar"])
	assertEq(t, Locations{Line(0), Line(2), Line(3)}, index["baz"])
}
