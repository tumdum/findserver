package main

import (
	"bufio"
	"io"
	"sort"
	"strings"

	"golang.org/x/text/unicode/norm"
)

type (
	Word        string
	Line        uint64
	Locations   []Line
	ReaderIndex map[Word]Locations
)

func NewReaderIndex() ReaderIndex {
	return make(ReaderIndex)
}

func (i ReaderIndex) known(word Word, line Line) bool {
	lines := i[word]
	return len(lines) != sort.Search(len(lines), func(i int) bool { return lines[i] == line })
}

func (i ReaderIndex) Add(word string, line Line) {
	w := Word(word)
	if !i.known(w, line) {
		i[w] = append(i[w], line)
	}
}

func normalize(s string) string {
	return norm.NFC.String(strings.ToLower(s))
}

type Storage map[Word]map[Path]Locations

type Index struct {
	storage Storage
}

func NewIndex() *Index {
	return &Index{
		storage: make(Storage),
	}
}

func (index *Index) Size() int {
	return len(index.storage)
}

func (index *Index) SetDocument(documentIndex ReaderIndex, path Path) {
	for word, locations := range documentIndex {
		if _, ok := index.storage[word]; !ok {
			index.storage[word] = make(map[Path]Locations)
		}
		index.storage[word][path] = locations
	}
}

func (index *Index) RemoveDocument(path Path) {
	for _, wordMap := range index.storage {
		delete(wordMap, path)
	}
}

func (index *Index) Lookup(word Word) (map[Path]Locations, bool) {
	wordMap, ok := index.storage[word]
	return wordMap, ok
}

func (index *Index) indexFromReader(r io.Reader) (ReaderIndex, error) {
	readerIndex := NewReaderIndex()
	s := bufio.NewScanner(bufio.NewReader(r))
	var line Line
	for s.Scan() {
		words := strings.Fields(normalize(s.Text()))
		for _, field := range words {
			readerIndex.Add(field, line)
		}
		line++
	}
	return readerIndex, nil
}
