package main

import (
	"io"
	"io/ioutil"
	"strings"
	"testing"
)

type stringSource struct {
	path    Path
	content string
}

func (s *stringSource) Open() (io.ReadCloser, error) {
	return ioutil.NopCloser(strings.NewReader(s.content)), nil
}

func (s *stringSource) Path() Path {
	return s.path
}

func TestSingleFileResultMatchesReaderIndex(t *testing.T) {
	input := "foo bar baz\nfoo\nbar baz\nbaz foo"
	path := Path("test.txt")
	server := NewServer()
	server.AddSource(&stringSource{path: path, content: input})

	result, err := server.search("foo")
	failTestOnError(t, err)
	expectedFooResult := SearchResult{path: Locations{Line(0), Line(1), Line(3)}}
	assertEq(t, expectedFooResult, result)

	result, err = server.search("bar")
	failTestOnError(t, err)
	expectedBarResults := SearchResult{path: Locations{Line(0), Line(2)}}
	assertEq(t, expectedBarResults, result)

	result, err = server.search("baz")
	failTestOnError(t, err)
	expectedBazResults := SearchResult{path: Locations{Line(0), Line(2), Line(3)}}
	assertEq(t, expectedBazResults, result)
}

func TestMultiFileResultIsSumOfSingleFileResults(t *testing.T) {
	server := NewServer()
	input := "foo bar baz\nfoo\nbar baz\nbaz foo"

	path := Path("test.txt")
	server.AddSource(&stringSource{path: path, content: input})

	path2 := Path("test2.txt")
	server.AddSource(&stringSource{path: path2, content: input})

	result, err := server.search("foo")
	failTestOnError(t, err)
	expectedFooResult := SearchResult{
		path:  Locations{Line(0), Line(1), Line(3)},
		path2: Locations{Line(0), Line(1), Line(3)}}
	assertEq(t, expectedFooResult, result)

	result, err = server.search("bar")
	failTestOnError(t, err)
	expectedBarResults := SearchResult{
		path:  Locations{Line(0), Line(2)},
		path2: Locations{Line(0), Line(2)}}
	assertEq(t, expectedBarResults, result)

	result, err = server.search("baz")
	failTestOnError(t, err)
	expectedBazResults := SearchResult{
		path:  Locations{Line(0), Line(2), Line(3)},
		path2: Locations{Line(0), Line(2), Line(3)}}
	assertEq(t, expectedBazResults, result)
}

func TestAfterRemovingFileThereShouldBeNoResultsFromIt(t *testing.T) {
	server := NewServer()
	input := "foo bar baz\nfoo\nbar baz\nbaz foo"

	path := Path("test.txt")
	server.AddSource(&stringSource{path: path, content: input})

	server.RemoveSource(path)

	for _, word := range []string{"foo", "bar", "baz"} {
		result, err := server.search(word)
		failTestOnError(t, err)
		if len(result) != 0 {
			t.Fatalf("Expected no results for '%v', got '%v'", word, result)
		}
	}
}

func TestCaseInsensitivity(t *testing.T) {
	server := NewServer()
	input := "Foo bar baz\nfoo\nbar baz\nbaz fOo"

	path := Path("test.txt")
	server.AddSource(&stringSource{path: path, content: input})
	for _, word := range []string{"foo", "Foo", "fOo"} {
		result, err := server.search(word)
		failTestOnError(t, err)
		assertEq(t, SearchResult{path: Locations{Line(0), Line(1), Line(3)}}, result)
	}
}

func TestUnicode(t *testing.T) {
	server := NewServer()
	input := "\u00e9abc\ne\u0301abc\nbar baz\nbaz fOo"
	path := Path("test.txt")
	server.AddSource(&stringSource{path: path, content: input})
	for _, word := range []string{"e\u0301abc", "\u00e9abc"} {
		result, err := server.search(word)
		failTestOnError(t, err)
		assertEq(t, SearchResult{path: Locations{Line(0), Line(1)}}, result)
	}
}

// TODO: add test for file removal
